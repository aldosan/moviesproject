using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;

namespace Movies.Api.Filters
{
    public class MovieExistsAttribute : Attribute, IAsyncResourceFilter
    {
        private readonly ApplicationDbContext _context;
        public MovieExistsAttribute(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            var movieIdObject = context.HttpContext.Request.RouteValues["movieId"];

            if (movieIdObject == null)
            {
                return;
            }

            var movieId = int.Parse(movieIdObject.ToString());

            if (!await _context.Movies.AnyAsync(x => x.Id == movieId))
            {
                context.Result = new NotFoundResult();
            }
            else
            {
                await next();
            }
        }
    }
}