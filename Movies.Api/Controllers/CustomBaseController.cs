using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Movies.Api.Helpers;

namespace Movies.Api.Controllers
{
    public class CustomBaseController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public CustomBaseController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        protected async Task<IActionResult> Get<TEntity, TDTO>() where TEntity : class
                                                                 where TDTO : class
        {
            var entities = await _context.Set<TEntity>().AsNoTracking().ToListAsync();
            var dtos = _mapper.Map<List<TDTO>>(entities);

            return Ok(dtos);
        }

        protected async Task<IActionResult> Get<TEntity, TDTO>(PaginationDTO paginationDTO) where TEntity : class
                                                                                            where TDTO : class
        {
            var queryable = _context.Set<TEntity>().AsQueryable();
            return await Get<TEntity, TDTO>(paginationDTO, queryable);
        }

        protected async Task<IActionResult> Get<TEntity, TDTO>(PaginationDTO paginationDTO,
                                                               IQueryable<TEntity> queryable)
            where TEntity : class
            where TDTO : class
        {
            await HttpContext.AddPaginationResponseHeader(queryable, paginationDTO.PageSize);

            var entities = await queryable.Paginate(paginationDTO).ToListAsync();
            var dtos = _mapper.Map<List<TDTO>>(entities);

            return Ok(dtos);
        }

        protected async Task<IActionResult> Get<TEntity, TDTO>(int id) where TEntity : class, IId
                                                                       where TDTO : class
        {
            var entity = await _context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                return NotFound();
            }

            var dto = _mapper.Map<TDTO>(entity);

            return Ok(dto);
        }

        protected async Task<IActionResult> Post<TCreation, TEntity, TDTO>(TCreation creationDTO, string routeName)
            where TCreation : class
            where TEntity : class, IId
            where TDTO : class
        {
            var entity = _mapper.Map<TEntity>(creationDTO);

            _context.Add(entity);
            await _context.SaveChangesAsync();

            var dto = _mapper.Map<TDTO>(entity);

            return CreatedAtRoute(routeName, new { id = entity.Id }, dto);
        }

        protected async Task<IActionResult> Patch<TEntity, TDTO>(int id, JsonPatchDocument<TDTO> patchDocument)
            where TDTO : class
            where TEntity : class, IId
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
            {
                return NotFound();
            }

            var entityDTO = _mapper.Map<TDTO>(entity);
            patchDocument.ApplyTo(entityDTO, ModelState);

            var isValid = TryValidateModel(entityDTO);
            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            _mapper.Map(entityDTO, entity);

            await _context.SaveChangesAsync();
            return NoContent();
        }

        protected async Task<IActionResult> Put<TCreation, TEntity>(int id, TCreation creationDTO)
            where TCreation : class
            where TEntity : class, IId
        {
            var entity = _mapper.Map<TEntity>(creationDTO);
            entity.Id = id;

            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        protected async Task<IActionResult> Delete<TEntity>(int id) where TEntity : class, IId, new()
        {
            if (!await _context.Set<TEntity>().AnyAsync(x => x.Id == id))
            {
                return NotFound();
            }

            _context.Remove(new TEntity { Id = id });
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}