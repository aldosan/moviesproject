using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Movies.Api.Filters;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/movies/{movieId:int}/reviews")]
    [ServiceFilter(typeof(MovieExistsAttribute))]
    [ApiController]
    public class ReviewsController : CustomBaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public ReviewsController(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int movieId, [FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = _context.Reviews.Include(x => x.User).AsQueryable();
            queryable = queryable.Where(x => x.MovieId == movieId);
            return await Get<Review, ReviewDTO>(paginationDTO, queryable);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Post(int movieId, [FromBody] ReviewForCreationDTO reviewForCreationDTO)
        {
            var userId = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

            if (await _context.Reviews.AnyAsync(x => x.MovieId == movieId && x.UserId == userId))
            {
                return BadRequest("There is already a created review from this user");
            }

            var review = _mapper.Map<Review>(reviewForCreationDTO);
            review.MovieId = movieId;
            review.UserId = userId;

            _context.Add(review);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpPut("{reviewId:int}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Put(int movieId, int reviewId, [FromBody] ReviewForCreationDTO reviewForCreationDTO)
        {
            var review = await _context.Reviews.FirstOrDefaultAsync(x => x.Id == reviewId);

            if (review == null) { return NotFound(); }

            var userId = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

            if (review.UserId != userId) { return BadRequest("This user is not allowed to edit this review"); }

            review = _mapper.Map(reviewForCreationDTO, review);

            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{reviewId:int}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete(int reviewId)
        {
            var review = await _context.Reviews.FirstOrDefaultAsync(x => x.Id == reviewId);

            if (review == null) { return NotFound(); }

            var userId = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

            if (review.UserId != userId) { return Forbid(); }

            _context.Remove(review);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
