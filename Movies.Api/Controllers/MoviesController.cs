using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Movies.Api.Helpers;
using Movies.Api.Services;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Logging;

namespace Movies.Api.Controllers
{
    [ApiController]
    [Route("api/v1/movies")]
    public class MoviesController : CustomBaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStorageService _storageService;
        private readonly string _container = "actors";
        private readonly ILogger<MoviesController> _logger;
        public MoviesController(ApplicationDbContext context,
                                IMapper mapper,
                                IStorageService storageService,
                                ILogger<MoviesController> logger) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
            _storageService = storageService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var nextReleases = await _context.Movies.Where(x => x.ReleaseDate > DateTime.Today)
                                                    .Take(5)
                                                    .ToListAsync();

            var inTheaters = await _context.Movies.Where(x => x.InTheaters)
                                                  .Take(5)
                                                  .ToListAsync();

            var movieIndexDTO = new MovieIndexDTO();
            movieIndexDTO.NextReleases = _mapper.Map<List<MovieDTO>>(nextReleases);
            movieIndexDTO.InTheaters = _mapper.Map<List<MovieDTO>>(inTheaters);

            return Ok(movieIndexDTO);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var movie = await _context.Movies
                                      .Include(x => x.MovieActors).ThenInclude(x => x.Actor)
                                      .Include(x => x.MovieGenres).ThenInclude(x => x.Genre)
                                      .FirstOrDefaultAsync(x => x.Id == id);
            if (movie == null)
            {
                return NotFound();
            }

            movie.MovieActors = movie.MovieActors.OrderBy(x => x.Order).ToList();

            var movieDetailDTO = _mapper.Map<MovieDetailDTO>(movie);

            return Ok(movieDetailDTO);
        }

        [HttpGet("filter")]
        public async Task<IActionResult> GetFilteredMovie([FromQuery] MovieFilterDTO movieParams)
        {
            var moviesQueryable = _context.Movies.AsQueryable();

            if (!string.IsNullOrEmpty(movieParams.Title))
            {
                moviesQueryable = moviesQueryable.Where(x => x.Title.Contains(movieParams.Title));
            }

            if (movieParams.InTheaters)
            {
                moviesQueryable = moviesQueryable.Where(x => x.InTheaters);
            }

            if (movieParams.NextReleases)
            {
                moviesQueryable = moviesQueryable.Where(x => x.ReleaseDate > DateTime.Today);
            }

            if (movieParams.GenreId > 0)
            {
                moviesQueryable = moviesQueryable.Where(x => x.MovieGenres.Select(y => y.GenreId)
                                                                          .Contains(movieParams.GenreId));
            }

            if (!string.IsNullOrEmpty(movieParams.OrderBy))
            {
                var orderType = movieParams.Ascending ? "ascending" : "descending";

                try
                {
                    moviesQueryable = moviesQueryable.OrderBy($"{movieParams.OrderBy.ToLower()} {orderType}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message, ex);
                }
            }

            await HttpContext.AddPaginationResponseHeader(moviesQueryable, movieParams.PageSize);

            var movies = await moviesQueryable.Paginate(movieParams.Pagination).ToListAsync();

            var moviesDTO = _mapper.Map<List<MovieDTO>>(movies);

            return Ok(moviesDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] MovieForCreationDTO movieForCreationDTO)
        {
            var movie = _mapper.Map<Movie>(movieForCreationDTO);

            if (movieForCreationDTO.Poster != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await movieForCreationDTO.Poster.CopyToAsync(memoryStream);
                    var content = memoryStream.ToArray();
                    var extension = Path.GetExtension(movieForCreationDTO.Poster.FileName);

                    movie.Poster = await _storageService.SaveFile(content, extension, _container, movieForCreationDTO.Poster.ContentType);
                }
            }

            AssignOrderToActor(movie);

            _context.Add(movie);
            await _context.SaveChangesAsync();

            var movieDTO = _mapper.Map<MovieDTO>(movie);

            return CreatedAtAction("Get", new { id = movieDTO.Id }, movieDTO);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromForm] MovieForCreationDTO movieForCreationDTO)
        {
            var movie = await _context.Movies
                                        .Include(x => x.MovieGenres)
                                        .Include(x => x.MovieActors)
                                        .FirstOrDefaultAsync(x => x.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            movie = _mapper.Map(movieForCreationDTO, movie);

            if (movieForCreationDTO.Poster != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await movieForCreationDTO.Poster.CopyToAsync(memoryStream);
                    var content = memoryStream.ToArray();
                    var extension = Path.GetExtension(movieForCreationDTO.Poster.FileName);

                    movie.Poster = await _storageService.EditFile(content, extension, _container, movie.Poster, movieForCreationDTO.Poster.ContentType);
                }
            }

            AssignOrderToActor(movie);

            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] JsonPatchDocument<MoviePatchDTO> patchDocument)
        {
            return await Patch<Movie, MoviePatchDTO>(id, patchDocument);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<Movie>(id);
        }

        private void AssignOrderToActor(Movie movie)
        {
            if (movie.MovieActors != null)
            {
                for (int i = 0; i < movie.MovieActors.Count; i++)
                {
                    movie.MovieActors[i].Order = i + 1;
                }
            }
        }
    }
}