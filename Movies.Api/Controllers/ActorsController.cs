using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Movies.Api.Services;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/actors")]
    [ApiController]
    public class ActorsController : CustomBaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStorageService _storageService;
        private readonly string _container = "actors";
        public ActorsController(ApplicationDbContext context,
                                IMapper mapper,
                                IStorageService storageService) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
            _storageService = storageService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PaginationDTO paginationDTO)
        {
            return await Get<Actor, ActorDTO>(paginationDTO);
        }

        [HttpGet("{id}", Name = "GetActorById")]
        public async Task<IActionResult> Get(int id)
        {
            return await Get<Actor, ActorDTO>(id);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] ActorForCreationDTO actorForCreationDTO)
        {
            var actor = _mapper.Map<Actor>(actorForCreationDTO);

            if (actorForCreationDTO.Photo != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await actorForCreationDTO.Photo.CopyToAsync(memoryStream);
                    var content = memoryStream.ToArray();
                    var extension = Path.GetExtension(actorForCreationDTO.Photo.FileName);

                    actor.Photo = await _storageService.SaveFile(content, extension, _container, actorForCreationDTO.Photo.ContentType);
                }
            }

            _context.Add(actor);
            await _context.SaveChangesAsync();

            var actorDTO = _mapper.Map<ActorDTO>(actor);

            return CreatedAtRoute("GetActorById", new { id = actorDTO.Id }, actorDTO);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromForm] ActorForCreationDTO actorForCreationDTO)
        {
            var actor = await _context.Actors.FirstOrDefaultAsync(x => x.Id == id);

            if (actor == null)
            {
                return NotFound();
            }

            actor = _mapper.Map(actorForCreationDTO, actor);

            if (actorForCreationDTO.Photo != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await actorForCreationDTO.Photo.CopyToAsync(memoryStream);
                    var content = memoryStream.ToArray();
                    var extension = Path.GetExtension(actorForCreationDTO.Photo.FileName);

                    actor.Photo = await _storageService.EditFile(content, extension, _container, actor.Photo, actorForCreationDTO.Photo.ContentType);
                }
            }

            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] JsonPatchDocument<ActorPatchDTO> patchDocument)
        {
            return await Patch<Actor, ActorPatchDTO>(id, patchDocument);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<Actor>(id);
        }
    }
}