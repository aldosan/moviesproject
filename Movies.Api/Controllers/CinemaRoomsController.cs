using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;
using Movies.Api.DTO;
using Movies.Api.Entities;
using NetTopologySuite.Geometries;

namespace Movies.Api.Controllers
{
    [Route("api/v1/cinemarooms")]
    [ApiController]
    public class CinemaRoomsController : CustomBaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly GeometryFactory _geometryFactory;
        public CinemaRoomsController(ApplicationDbContext context,
                                     IMapper mapper,
                                     GeometryFactory geometryFactory) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
            _geometryFactory = geometryFactory;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Get<CinemaRoom, CinemaRoomDTO>();
        }

        [HttpGet("{id:int}", Name = "GetCinemaRoomById")]
        public async Task<IActionResult> Get(int id)
        {
            return await Get<CinemaRoom, CinemaRoomDTO>(id);
        }

        [HttpGet("distance")]
        public async Task<IActionResult> GetByDistance([FromQuery] CinemaRoomDistanceFilterDTO cinemaRoomDistanceFilterDTO)
        {
            var userLocation = _geometryFactory.CreatePoint(new Coordinate(cinemaRoomDistanceFilterDTO.Longitude,
                                                                           cinemaRoomDistanceFilterDTO.Latitude));
            var cinemaRooms = await _context.CinemaRooms
                                            .OrderBy(x => x.Location.Distance(userLocation))
                                            .Where(x => x.Location.IsWithinDistance(userLocation,
                                                                                    cinemaRoomDistanceFilterDTO.DistanceKms * 1000))
                                            .Select(x => new CinemaRoomDistanceDTO
                                            {
                                                Id = x.Id,
                                                Name = x.Name,
                                                Latitude = x.Location.Y,
                                                Longitude = x.Location.X,
                                                DistanceMts = Math.Round(x.Location.Distance(userLocation))
                                            }).ToListAsync();

            return Ok(cinemaRooms);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CinemaRoomForCreationDTO cinemaRoomForCreationDTO)
        {
            return await Post<CinemaRoomForCreationDTO, CinemaRoom, CinemaRoomDTO>(cinemaRoomForCreationDTO, "GetCinemaRoomById");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CinemaRoomForCreationDTO cinemaRoomForCreationDTO)
        {
            return await Put<CinemaRoomForCreationDTO, CinemaRoom>(id, cinemaRoomForCreationDTO);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<CinemaRoom>(id);
        }
    }
}