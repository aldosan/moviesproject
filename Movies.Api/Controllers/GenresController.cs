using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Movies.Api.Context;
using Movies.Api.DTO;
using Movies.Api.Entities;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/genres")]
    [ApiController]
    public class GenresController : CustomBaseController
    {
        private readonly ApplicationDbContext _context;
        public GenresController(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Get<Genre, GenreDTO>();
        }

        [HttpGet("{id}", Name = "GetGenreById")]
        public async Task<IActionResult> Get(int id)
        {
            return await Get<Genre, GenreDTO>(id);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GenreForCreationDTO genreForCreationDTO)
        {
            return await Post<GenreForCreationDTO, Genre, GenreDTO>(genreForCreationDTO, "GetGenreById");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] GenreForCreationDTO genreForCreationDTO)
        {
            return await Put<GenreForCreationDTO, Genre>(id, genreForCreationDTO);
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete(int id)
        {
            return await Delete<Genre>(id);
        }
    }
}