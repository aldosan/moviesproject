using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NetTopologySuite.Geometries;

namespace Movies.Api.Entities
{
    public class CinemaRoom : IId
    {
        public int Id { get; set; }

        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        public Point Location { get; set; }
        public List<MovieCinemaRoom> MovieCinemaRooms { get; set; }
    }
}