using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Movies.Api.Entities
{
    public class Actor : IId
    {
        public int Id { get; set; }

        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Photo { get; set; }
        public List<MovieActor> MovieActors { get; set; }
    }
}