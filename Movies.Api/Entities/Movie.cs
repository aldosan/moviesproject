using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Movies.Api.Entities
{
    public class Movie : IId
    {
        public int Id { get; set; }

        [Required]
        [StringLength(300)]
        public string Title { get; set; }
        public bool InTheaters { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Poster { get; set; }
        public List<MovieGenre> MovieGenres { get; set; }
        public List<MovieActor> MovieActors { get; set; }
        public List<MovieCinemaRoom> MovieCinemaRooms { get; set; }
    }
}