namespace Movies.Api.Entities
{
    public interface IId
    {
        public int Id { get; set; }
    }
}