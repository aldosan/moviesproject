using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Movies.Api.Validations
{
    public class FileSizeValidator : ValidationAttribute
    {
        private readonly int _maxSizeMB;

        public FileSizeValidator(int maxSizeMB)
        {
            _maxSizeMB = maxSizeMB;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            IFormFile formFile = value as IFormFile;
            if (formFile == null)
            {
                return ValidationResult.Success;
            }

            if (formFile.Length > _maxSizeMB * 1024 * 1024)
            {
                return new ValidationResult($"File Size should not be greater than {_maxSizeMB}MB");
            }
            return ValidationResult.Success;
        }
    }
}