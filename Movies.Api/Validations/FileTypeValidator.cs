using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Movies.Api.Enums;

namespace Movies.Api.Validations
{
    public class FileTypeValidator : ValidationAttribute
    {
        private readonly string[] _validFileTypes;
        public FileTypeValidator(string[] validFileTypes)
        {
            _validFileTypes = validFileTypes;
        }

        public FileTypeValidator(FileTypeGroup validFileTypes)
        {
            if (validFileTypes == FileTypeGroup.Image)
            {
                _validFileTypes = new string[] { "image/jpeg", "image/png", "image/gif" };
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            IFormFile formFile = value as IFormFile;
            if (formFile == null)
            {
                return ValidationResult.Success;
            }

            if (!_validFileTypes.Contains(formFile.ContentType))
            {
                return new ValidationResult($"File type should be one the following: {string.Join(", ", _validFileTypes)}");
            }
            return ValidationResult.Success;
        }
    }
}