using System.Threading.Tasks;

namespace Movies.Api.Services
{
    public interface IStorageService
    {
        Task<string> EditFile(byte[] content, string extension, string container, string path, string contentType);
        Task DeleteFile(string container, string path);
        Task<string> SaveFile(byte[] content, string extension, string container, string contentType);
    }
}