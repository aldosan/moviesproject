using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Movies.Api.Services
{
    public class LocalStorageService : IStorageService
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public LocalStorageService(IWebHostEnvironment environment, IHttpContextAccessor httpContextAccessor)
        {
            _environment = environment;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task DeleteFile(string container, string path)
        {
            if (path != null)
            {
                var filename = Path.GetFileName(path);
                var fileDirectory = Path.Combine(_environment.WebRootPath, container, filename);

                if (File.Exists(fileDirectory))
                {
                    File.Delete(fileDirectory);
                }
            }
            return Task.FromResult(0);
        }

        public async Task<string> EditFile(byte[] content, string extension, string container, string path, string contentType)
        {
            await DeleteFile(container, path);
            return await SaveFile(content, extension, container, contentType);
        }

        public async Task<string> SaveFile(byte[] content, string extension, string container, string contentType)
        {
            var filename = $"{Guid.NewGuid()}{extension}";
            var folder = Path.Combine(_environment.WebRootPath, container);

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            var path = Path.Combine(folder, filename);
            await File.WriteAllBytesAsync(path, content);

            var currentUrl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
            var urlForDatabase = Path.Combine(currentUrl, container, filename).Replace("\\", "/");

            return urlForDatabase;
        }
    }
}