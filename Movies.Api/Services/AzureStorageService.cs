using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Configuration;

namespace Movies.Api.Services
{
    public class AzureStorageService : IStorageService
    {
        private readonly string _connectionString;
        public AzureStorageService(IConfiguration config)
        {
            _connectionString = config.GetConnectionString("AzureStorage");
        }

        public async Task DeleteFile(string container, string path)
        {
            if (path != null)
            {
                var account = CloudStorageAccount.Parse(_connectionString);
                var client = account.CreateCloudBlobClient();
                var containerReference = client.GetContainerReference(container);

                var blobName = Path.GetFileName(path);
                var blob = containerReference.GetBlobReference(blobName);
                await blob.DeleteIfExistsAsync();
            }
        }

        public async Task<string> EditFile(byte[] content, string extension, string container, string path, string contentType)
        {
            await DeleteFile(container, path);
            return await SaveFile(content, extension, container, contentType);
        }

        public async Task<string> SaveFile(byte[] content, string extension, string container, string contentType)
        {
            var account = CloudStorageAccount.Parse(_connectionString);
            var client = account.CreateCloudBlobClient();
            var containerReference = client.GetContainerReference(container);

            await containerReference.CreateIfNotExistsAsync();
            await containerReference.SetPermissionsAsync(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });

            var fileName = $"{Guid.NewGuid()}{extension}";
            var blob = containerReference.GetBlockBlobReference(fileName);

            await blob.UploadFromByteArrayAsync(content, 0, content.Length);
            blob.Properties.ContentType = contentType;
            await blob.SetPropertiesAsync();

            return blob.Uri.ToString();
        }
    }
}