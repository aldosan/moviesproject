using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Movies.Api.Entities;
using NetTopologySuite;
using NetTopologySuite.Geometries;

namespace Movies.Api.Context
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieActor>()
                .HasKey(x => new { x.MovieId, x.ActorId });

            modelBuilder.Entity<MovieGenre>()
                .HasKey(x => new { x.MovieId, x.GenreId });

            modelBuilder.Entity<MovieCinemaRoom>()
                .HasKey(x => new { x.MovieId, x.CinemaRoomId });

            SeedData(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            var AdminRoleId = "83678112-fb7a-4e84-b1a2-f4c68e50b22b";
            var AdminUserId = "99731322-eb6e-407e-b9ed-7a5c66219512";

            var AdminRole = new IdentityRole
            {
                Id = AdminRoleId,
                Name = "Admin",
                NormalizedName = "Admin"
            };

            var passwordHash = new PasswordHasher<IdentityUser>();
            var username = "alfredo.sanchez";
            var email = "alfredo.2993@gmail.com";

            var AdminUser = new IdentityUser
            {
                Id = AdminUserId,
                UserName = username,
                NormalizedUserName = username,
                Email = email,
                NormalizedEmail = email,
                PasswordHash = passwordHash.HashPassword(null, "AbCdE123456!")
            };

            modelBuilder.Entity<IdentityUser>()
                .HasData(AdminUser);

            modelBuilder.Entity<IdentityRole>()
                .HasData(AdminRole);

            modelBuilder.Entity<IdentityUserClaim<string>>()
                .HasData(new IdentityUserClaim<string>
                {
                    Id = 1,
                    ClaimType = ClaimTypes.Role,
                    UserId = AdminUserId,
                    ClaimValue = "Admin"
                });

            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);

            modelBuilder.Entity<CinemaRoom>()
                .HasData(new List<CinemaRoom>
                {
                    new CinemaRoom{Id = 1, Name = "Agora", Location = geometryFactory.CreatePoint(new Coordinate(-69.9388777, 18.4839233))},
                    new CinemaRoom{Id = 2, Name = "Sambil", Location = geometryFactory.CreatePoint(new Coordinate(-69.9118804, 18.4826214))},
                    new CinemaRoom{Id = 3, Name = "Megacentro", Location = geometryFactory.CreatePoint(new Coordinate(-69.856427, 18.506934))},
                    new CinemaRoom{Id = 4, Name = "Village East Cinema", Location = geometryFactory.CreatePoint(new Coordinate(-73.986227, 40.730898))}
                });

            var adventure = new Genre() { Id = 1, Name = "Adventure" };
            var animation = new Genre() { Id = 2, Name = "Animation" };
            var drama = new Genre() { Id = 3, Name = "Drama" };
            var romance = new Genre() { Id = 4, Name = "Romance" };

            modelBuilder.Entity<Genre>()
                .HasData(new List<Genre>
                {
                    adventure, animation, drama, romance
                });

            var jimCarrey = new Actor() { Id = 1, Name = "Jim Carrey", BirthDate = new DateTime(1962, 01, 17) };
            var robertDowney = new Actor() { Id = 2, Name = "Robert Downey Jr.", BirthDate = new DateTime(1965, 4, 4) };
            var chrisEvans = new Actor() { Id = 3, Name = "Chris Evans", BirthDate = new DateTime(1981, 06, 13) };

            modelBuilder.Entity<Actor>()
                .HasData(new List<Actor>
                {
                    jimCarrey, robertDowney, chrisEvans
                });

            var endgame = new Movie()
            {
                Id = 1,
                Title = "Avengers: Endgame",
                InTheaters = true,
                ReleaseDate = new DateTime(2019, 04, 26)
            };

            var iw = new Movie()
            {
                Id = 2,
                Title = "Avengers: Infinity Wars",
                InTheaters = false,
                ReleaseDate = new DateTime(2019, 04, 26)
            };

            var sonic = new Movie()
            {
                Id = 3,
                Title = "Sonic the Hedgehog",
                InTheaters = false,
                ReleaseDate = new DateTime(2020, 02, 28)
            };
            var emma = new Movie()
            {
                Id = 4,
                Title = "Emma",
                InTheaters = false,
                ReleaseDate = new DateTime(2020, 02, 21)
            };
            var greed = new Movie()
            {
                Id = 5,
                Title = "Greed",
                InTheaters = false,
                ReleaseDate = new DateTime(2020, 02, 21)
            };

            modelBuilder.Entity<Movie>()
                .HasData(new List<Movie>
                {
                    endgame, iw, sonic, emma, greed
                });

            modelBuilder.Entity<MovieGenre>().HasData(
                new List<MovieGenre>()
                {
                    new MovieGenre(){MovieId = endgame.Id, GenreId = drama.Id},
                    new MovieGenre(){MovieId = endgame.Id, GenreId = adventure.Id},
                    new MovieGenre(){MovieId = iw.Id, GenreId = drama.Id},
                    new MovieGenre(){MovieId = iw.Id, GenreId = adventure.Id},
                    new MovieGenre(){MovieId = sonic.Id, GenreId = adventure.Id},
                    new MovieGenre(){MovieId = emma.Id, GenreId = drama.Id},
                    new MovieGenre(){MovieId = emma.Id, GenreId = romance.Id},
                    new MovieGenre(){MovieId = greed.Id, GenreId = drama.Id},
                    new MovieGenre(){MovieId = greed.Id, GenreId = romance.Id},
                });

            modelBuilder.Entity<MovieActor>().HasData(
                new List<MovieActor>()
                {
                    new MovieActor(){MovieId = endgame.Id, ActorId = robertDowney.Id, Character = "Tony Stark", Order = 1},
                    new MovieActor(){MovieId = endgame.Id, ActorId = chrisEvans.Id, Character = "Steve Rogers", Order = 2},
                    new MovieActor(){MovieId = iw.Id, ActorId = robertDowney.Id, Character = "Tony Stark", Order = 1},
                    new MovieActor(){MovieId = iw.Id, ActorId = chrisEvans.Id, Character = "Steve Rogers", Order = 2},
                    new MovieActor(){MovieId = sonic.Id, ActorId = jimCarrey.Id, Character = "Dr. Ivo Robotnik", Order = 1}
                });
        }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<CinemaRoom> CinemaRooms { get; set; }
        public DbSet<MovieGenre> MovieGenres { get; set; }
        public DbSet<MovieActor> MovieActors { get; set; }
        public DbSet<MovieCinemaRoom> MovieCinemaRooms { get; set; }
        public DbSet<Review> Reviews { get; set; }
    }
}