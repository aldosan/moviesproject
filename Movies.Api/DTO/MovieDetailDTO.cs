using System.Collections.Generic;

namespace Movies.Api.DTO
{
    public class MovieDetailDTO : MovieDTO
    {
        public List<GenreDTO> Genres { get; set; }
        public List<MovieActorDetailDTO> Actors { get; set; }
    }
}