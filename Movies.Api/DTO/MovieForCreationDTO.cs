using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Enums;
using Movies.Api.Helpers;
using Movies.Api.Validations;

namespace Movies.Api.DTO
{
    public class MovieForCreationDTO
    {
        [Required]
        [StringLength(300)]
        public string Title { get; set; }
        public bool IsReleased { get; set; }
        public DateTime ReleaseDate { get; set; }

        [FileSizeValidator(maxSizeMB: 4)]
        [FileTypeValidator(validFileTypes: FileTypeGroup.Image)]
        public IFormFile Poster { get; set; }

        [ModelBinder(BinderType = typeof(TypeBinder<List<int>>))]
        public List<int> GenreIds { get; set; }

        [ModelBinder(BinderType = typeof(TypeBinder<List<MovieActorForCreationDTO>>))]
        public List<MovieActorForCreationDTO> Actors { get; set; }
    }
}