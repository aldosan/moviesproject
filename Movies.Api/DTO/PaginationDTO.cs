namespace Movies.Api.DTO
{
    public class PaginationDTO
    {
        public int Page { get; set; } = 1;
        private readonly int maxPageSize = 50;
        private int pageSize = 10;
        public int PageSize
        {
            get => pageSize;
            set
            {
                pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
    }
}