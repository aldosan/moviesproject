namespace Movies.Api.DTO
{
    public class MovieActorDetailDTO
    {
        public int ActorId { get; set; }
        public string Character { get; set; }
        public string ActorName { get; set; }
    }
}