using System;

namespace Movies.Api.DTO
{
    public class ActorPatchDTO
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}