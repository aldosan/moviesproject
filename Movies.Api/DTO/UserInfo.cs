using System.ComponentModel.DataAnnotations;

namespace Movies.Api.DTO
{
    public class UserInfo
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}