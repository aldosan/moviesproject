using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Movies.Api.Enums;
using Movies.Api.Validations;

namespace Movies.Api.DTO
{
    public class ActorForCreationDTO
    {
        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }

        [FileSizeValidator(maxSizeMB: 4)]
        [FileTypeValidator(validFileTypes: FileTypeGroup.Image)]
        public IFormFile Photo { get; set; }
    }
}