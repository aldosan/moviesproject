namespace Movies.Api.DTO
{
    public class MovieFilterDTO
    {
        public int Page { get; set; } = 1;
        public int PageSize = 10;
        public PaginationDTO Pagination
        {
            get
            {
                return new PaginationDTO { Page = Page, PageSize = PageSize };
            }
        }

        public string Title { get; set; }
        public int GenreId { get; set; }
        public bool InTheaters { get; set; }
        public bool NextReleases { get; set; }
        public string OrderBy { get; set; }
        public bool Ascending { get; set; } = true;
    }
}