using System.ComponentModel.DataAnnotations;

namespace Movies.Api.DTO
{
    public class CinemaRoomDistanceFilterDTO
    {
        [Range(-90, 90)]
        public double Latitude { get; set; }

        [Range(-180, 180)]
        public double Longitude { get; set; }
        private int distanceKms = 10;
        private readonly int maxDistanceKms = 50;

        public int DistanceKms
        {
            get
            {
                return distanceKms;
            }
            set
            {
                distanceKms = (value > maxDistanceKms) ? maxDistanceKms : value;
            }
        }
    }
}