using System.Collections.Generic;

namespace Movies.Api.DTO
{
    public class MovieIndexDTO
    {
        public List<MovieDTO> NextReleases { get; set; }
        public List<MovieDTO> InTheaters { get; set; }
    }
}