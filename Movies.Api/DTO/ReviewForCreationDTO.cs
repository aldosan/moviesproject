using System.ComponentModel.DataAnnotations;

namespace Movies.Api.DTO
{
    public class ReviewForCreationDTO
    {
        public string Comment { get; set; }

        [Range(1, 5)]
        public int Score { get; set; }
    }
}