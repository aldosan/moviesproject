namespace Movies.Api.DTO
{
    public class MovieActorForCreationDTO
    {
        public int ActorId { get; set; }
        public string Character { get; set; }
    }
}