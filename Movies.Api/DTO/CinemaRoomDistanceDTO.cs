namespace Movies.Api.DTO
{
    public class CinemaRoomDistanceDTO : CinemaRoomDTO
    {
        public double DistanceMts { get; set; }
    }
}