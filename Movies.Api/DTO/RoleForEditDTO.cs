namespace Movies.Api.DTO
{
    public class RoleForEditDTO
    {
        public string UserId { get; set; }
        public string RoleName { get; set; }
    }
}