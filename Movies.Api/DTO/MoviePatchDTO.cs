using System;

namespace Movies.Api.DTO
{
    public class MoviePatchDTO
    {
        public string Title { get; set; }
        public bool IsReleased { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}