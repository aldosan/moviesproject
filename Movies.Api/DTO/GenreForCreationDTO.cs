using System.ComponentModel.DataAnnotations;

namespace Movies.Api.DTO
{
    public class GenreForCreationDTO
    {
        [Required]
        [StringLength(40)]
        public string Name { get; set; }
    }
}