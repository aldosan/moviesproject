using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Movies.Api.Helpers
{
    public static class HttpContextExtensions
    {
        public async static Task AddPaginationResponseHeader<T>(this HttpContext httpContext,
                                                                IQueryable<T> queryable,
                                                                int pageSize)
        {
            double records = await queryable.CountAsync();
            double pages = Math.Ceiling(records / pageSize);
            httpContext.Response.Headers.Add("pages", pages.ToString());
        }
    }
}