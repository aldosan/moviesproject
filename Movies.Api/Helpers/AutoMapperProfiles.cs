using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Movies.Api.DTO;
using Movies.Api.Entities;
using NetTopologySuite.Geometries;

namespace Movies.Api.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles(GeometryFactory geometryFactory)
        {
            CreateMap<Genre, GenreDTO>().ReverseMap();
            CreateMap<GenreForCreationDTO, Genre>();

            CreateMap<IdentityUser, UserDTO>();

            CreateMap<CinemaRoom, CinemaRoomDTO>()
                .ForMember(x => x.Latitude, x => x.MapFrom(y => y.Location.Y))
                .ForMember(x => x.Longitude, x => x.MapFrom(y => y.Location.X));

            CreateMap<CinemaRoomDTO, CinemaRoom>()
                .ForMember(x => x.Location,
                           x => x.MapFrom(y => geometryFactory.CreatePoint(new Coordinate(y.Longitude, y.Latitude))));

            CreateMap<CinemaRoomForCreationDTO, CinemaRoom>()
                .ForMember(x => x.Location,
                           x => x.MapFrom(y => geometryFactory.CreatePoint(new Coordinate(y.Longitude, y.Latitude))));

            CreateMap<Actor, ActorDTO>().ReverseMap();
            CreateMap<ActorForCreationDTO, Actor>()
                .ForMember(x => x.Photo, options => options.Ignore());

            CreateMap<ActorPatchDTO, Actor>().ReverseMap();

            CreateMap<Movie, MovieDTO>().ReverseMap();
            CreateMap<MovieForCreationDTO, Movie>()
                .ForMember(x => x.Poster, options => options.Ignore())
                .ForMember(x => x.MovieGenres, options => options.MapFrom(MapMovieGenres))
                .ForMember(x => x.MovieActors, options => options.MapFrom(MapMovieActor));

            CreateMap<Movie, MovieDetailDTO>()
                .ForMember(x => x.Genres, options => options.MapFrom(MapMovieGenres))
                .ForMember(x => x.Actors, options => options.MapFrom(MapMovieActors));

            CreateMap<MoviePatchDTO, Movie>().ReverseMap();

            CreateMap<Review, ReviewDTO>()
                .ForMember(x => x.Username, options => options.MapFrom(x => x.User.UserName));

            CreateMap<ReviewDTO, Review>();
            CreateMap<ReviewForCreationDTO, Review>();
        }

        private List<GenreDTO> MapMovieGenres(Movie movie, MovieDetailDTO movieDetailDTO)
        {
            var result = new List<GenreDTO>();

            if (movie.MovieGenres == null)
            {
                return result;
            }

            foreach (var movieGenre in movie.MovieGenres)
            {
                result.Add(new GenreDTO
                {
                    Id = movieGenre.GenreId,
                    Name = movieGenre.Genre.Name
                });
            }
            return result;
        }

        private List<MovieActorDetailDTO> MapMovieActors(Movie movie, MovieDetailDTO movieDetailDTO)
        {
            var result = new List<MovieActorDetailDTO>();

            if (movie.MovieActors == null)
            {
                return result;
            }

            foreach (var movieActor in movie.MovieActors)
            {
                result.Add(new MovieActorDetailDTO
                {
                    ActorId = movieActor.ActorId,
                    Character = movieActor.Character,
                    ActorName = movieActor.Actor.Name
                });
            }
            return result;
        }

        private List<MovieGenre> MapMovieGenres(MovieForCreationDTO movieForCreation, Movie movie)
        {
            var result = new List<MovieGenre>();

            if (movieForCreation.GenreIds == null)
            {
                return result;
            }

            foreach (var id in movieForCreation.GenreIds)
            {
                result.Add(new MovieGenre
                {
                    GenreId = id
                });
            }
            return result;
        }

        private List<MovieActor> MapMovieActor(MovieForCreationDTO movieForCreation, Movie movie)
        {
            var result = new List<MovieActor>();

            if (movieForCreation.Actors == null)
            {
                return result;
            }

            foreach (var actor in movieForCreation.Actors)
            {
                result.Add(new MovieActor
                {
                    ActorId = actor.ActorId,
                    Character = actor.Character
                });
            }
            return result;
        }
    }
}