using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Movies.Api.Controllers;
using Movies.Api.DTO;
using Movies.Api.Entities;

namespace Movies.UnitTests
{
    [TestClass]
    public class GenresControllerTests : BaseTests
    {
        [TestMethod]
        public async Task GetAllGenres()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            context.Genres.Add(new Genre { Name = "Genre 1" });
            context.Genres.Add(new Genre { Name = "Genre 2" });
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);

            //Act
            var controller = new GenresController(newContext, mapper);
            var response = await controller.Get();

            //Assert
            var okResult = response as OkObjectResult;
            var genres = okResult.Value as List<GenreDTO>;

            Assert.AreEqual(2, genres.Count);
        }

        [TestMethod]
        public async Task GetGenreByNonExistentId()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            //Act
            var controller = new GenresController(context, mapper);
            var response = await controller.Get(1);

            //Assert
            var result = response as NotFoundResult;
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public async Task GetGenreByExistentId()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            context.Genres.Add(new Genre { Name = "Genre 1" });
            context.Genres.Add(new Genre { Name = "Genre 2" });
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);

            //Act
            var controller = new GenresController(newContext, mapper);
            var response = await controller.Get(1);

            //Assert
            var okResult = response as OkObjectResult;
            var genre = okResult.Value as GenreDTO;
            Assert.AreEqual(1, genre.Id);
        }

        [TestMethod]
        public async Task CreateGenre()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            var newGenre = new GenreForCreationDTO { Name = "New Genre" };

            //Act
            var controller = new GenresController(context, mapper);
            var response = await controller.Post(newGenre);

            //Assert
            var result = response as CreatedAtRouteResult;
            Assert.IsNotNull(result);

            var newContext = BuildContext(dbName);
            var count = await newContext.Genres.CountAsync();
            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public async Task UpdateGenre()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            context.Genres.Add(new Genre { Name = "Genre 1" });
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);

            //Act
            var controller = new GenresController(newContext, mapper);

            var id = 1;
            var newName = "New Genre 1";
            var genreForCreationDTO = new GenreForCreationDTO { Name = newName };

            var response = await controller.Put(id, genreForCreationDTO);

            //Assert
            var result = response as StatusCodeResult;
            Assert.AreEqual(204, result.StatusCode);

            var secondNewContext = BuildContext(dbName);
            var exists = await secondNewContext.Genres.AnyAsync(x => x.Name == newName);
            Assert.IsTrue(exists);
        }

        [TestMethod]
        public async Task DeleteNonExistentGenre()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            //Act
            var controller = new GenresController(context, mapper);
            var response = await controller.Delete(1);

            //Assert
            var result = response as StatusCodeResult;
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public async Task DeleteGenre()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            context.Genres.Add(new Genre { Name = "Genre 1" });
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);

            //Act
            var controller = new GenresController(newContext, mapper);
            var response = await controller.Delete(1);

            //Assert
            var result = response as StatusCodeResult;
            Assert.AreEqual(204, result.StatusCode);

            var secondNewContext = BuildContext(dbName);
            var exists = await secondNewContext.Genres.AnyAsync();
            Assert.IsFalse(exists);
        }
    }
}