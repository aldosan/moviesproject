using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Movies.Api.Controllers;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Movies.Api.Services;

namespace Movies.UnitTests
{
    [TestClass]
    public class ActorsControllerTests : BaseTests
    {
        [TestMethod]
        public async Task GetPaginatedActors()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            context.Actors.Add(new Actor { Name = "Actor 1" });
            context.Actors.Add(new Actor { Name = "Actor 2" });
            context.Actors.Add(new Actor { Name = "Actor 3" });
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);

            //Act
            var controller = new ActorsController(newContext, mapper, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var responsePage1 = await controller.Get(new PaginationDTO { Page = 1, PageSize = 2 });

            //Assert
            var resultPage1 = responsePage1 as OkObjectResult;
            var actorsPage1 = resultPage1.Value as List<ActorDTO>;
            Assert.AreEqual(2, actorsPage1.Count);

            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var responsePage2 = await controller.Get(new PaginationDTO { Page = 2, PageSize = 2 });

            var resultPage2 = responsePage2 as OkObjectResult;
            var actorsPage2 = resultPage2.Value as List<ActorDTO>;
            Assert.AreEqual(1, actorsPage2.Count);

            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var responsePage3 = await controller.Get(new PaginationDTO { Page = 3, PageSize = 2 });

            var resultPage3 = responsePage3 as OkObjectResult;
            var actorsPage3 = resultPage3.Value as List<ActorDTO>;
            Assert.AreEqual(0, actorsPage3.Count);
        }

        [TestMethod]
        public async Task CreateActorWithoutPhoto()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            var actor = new ActorForCreationDTO { Name = "Alfredo", BirthDate = DateTime.Now };

            var mock = new Mock<IStorageService>();
            mock.Setup(x => x.SaveFile(null, null, null, null))
                .Returns(Task.FromResult("url"));

            //Act
            var controller = new ActorsController(context, mapper, mock.Object);
            var response = await controller.Post(actor);

            //Assert
            var result = response as CreatedAtRouteResult;
            Assert.AreEqual(201, result.StatusCode);

            var newContext = BuildContext(dbName);
            var list = await newContext.Actors.ToListAsync();
            Assert.AreEqual(1, list.Count);
            Assert.IsNull(list[0].Photo);
            Assert.AreEqual(0, mock.Invocations.Count);
        }

        [TestMethod]
        public async Task CreateActorWithPhoto()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            var content = Encoding.UTF8.GetBytes("Test Image");
            var file = new FormFile(new MemoryStream(content), 0, content.Length, "Data", "image.jpg");
            file.Headers = new HeaderDictionary();
            file.ContentType = "image/jpg";

            var actor = new ActorForCreationDTO
            {
                Name = "Alfredo",
                BirthDate = DateTime.Now,
                Photo = file
            };

            var mock = new Mock<IStorageService>();
            mock.Setup(x => x.SaveFile(content, ".jpg", "actors", file.ContentType))
                .Returns(Task.FromResult("url"));

            //Act
            var controller = new ActorsController(context, mapper, mock.Object);
            var response = await controller.Post(actor);

            //Assert
            var result = response as CreatedAtRouteResult;
            Assert.AreEqual(201, result.StatusCode);

            var newContext = BuildContext(dbName);
            var list = await newContext.Actors.ToListAsync();
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual("url", list[0].Photo);
            Assert.AreEqual(1, mock.Invocations.Count);
        }

        [TestMethod]
        public async Task PatchNonExistentActor()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            //Act
            var controller = new ActorsController(context, mapper, null);
            var patchDoc = new JsonPatchDocument<ActorPatchDTO>();
            var response = await controller.Patch(1, patchDoc);

            //Assert
            var result = response as StatusCodeResult;
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public async Task PatchOnlyOneAttribute()
        {
            //Arrange
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = BuildMapper();

            var birthDate = DateTime.Now;
            var actor = new Actor { Name = "Alfredo", BirthDate = birthDate };

            context.Add(actor);
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);

            //Act
            var controller = new ActorsController(newContext, mapper, null);

            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(x => x.Validate(It.IsAny<ActionContext>(),
                                                  It.IsAny<ValidationStateDictionary>(),
                                                  It.IsAny<string>(),
                                                  It.IsAny<object>()));

            controller.ObjectValidator = objectValidator.Object;

            var patchDoc = new JsonPatchDocument<ActorPatchDTO>();
            patchDoc.Operations.Add(new Operation<ActorPatchDTO>("replace", "/name", null, "Llicci"));

            var response = await controller.Patch(1, patchDoc);

            //Assert
            var result = response as StatusCodeResult;
            Assert.AreEqual(204, result.StatusCode);

            var secondNewContext = BuildContext(dbName);
            var patchedActor = await secondNewContext.Actors.FirstOrDefaultAsync();
            Assert.AreEqual("Llicci", patchedActor.Name);
            Assert.AreEqual(birthDate, patchedActor.BirthDate);
        }
    }
}