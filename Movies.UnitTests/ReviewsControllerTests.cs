using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Movies.Api.Controllers;
using Movies.Api.DTO;
using Movies.Api.Entities;

namespace Movies.UnitTests
{
    [TestClass]
    public class ReviewsControllerTests : BaseTests
    {
        [TestMethod]
        public async Task UserCannotCreateTwoReviewsForTheSameMovie()
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            CreateMovies(dbName);

            var movieId = context.Movies.Select(x => x.Id).First();
            var review = new Review
            {
                MovieId = movieId,
                UserId = _defaultUserId,
                Score = 5
            };

            context.Add(review);
            await context.SaveChangesAsync();

            var newContext = BuildContext(dbName);
            var mapper = BuildMapper();

            var controller = new ReviewsController(newContext, mapper);
            controller.ControllerContext = BuildControllerContext();

            var reviewForCreationDTO = new ReviewForCreationDTO
            {
                Score = 5
            };

            var response = await controller.Post(movieId, reviewForCreationDTO);
            var result = response as IStatusCodeActionResult;

            Assert.AreEqual(400, result.StatusCode);
        }

        [TestMethod]
        public async Task CreateReview()
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            CreateMovies(dbName);

            var movieId = context.Movies.Select(x => x.Id).First();
            var newContext = BuildContext(dbName);

            var mapper = BuildMapper();
            var controller = new ReviewsController(newContext, mapper);
            controller.ControllerContext = BuildControllerContext();

            var reviewForCreationDTO = new ReviewForCreationDTO
            {
                Score = 5
            };

            var response = await controller.Post(movieId, reviewForCreationDTO);
            var result = response as NoContentResult;
            Assert.IsNotNull(result);

            var secondNewContext = BuildContext(dbName);
            var reviewFromDB = secondNewContext.Reviews.First();
            Assert.AreEqual(_defaultUserId, reviewFromDB.UserId);
        }

        private void CreateMovies(string dbName)
        {
            var context = BuildContext(dbName);
            context.Movies.Add(new Movie { Title = "Movie 1" });
            context.SaveChanges();
        }
    }
}