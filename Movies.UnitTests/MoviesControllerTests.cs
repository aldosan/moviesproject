using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Movies.Api.Controllers;
using Movies.Api.DTO;
using Movies.Api.Entities;

namespace Movies.UnitTests
{
    [TestClass]
    public class MoviesControllerTests : BaseTests
    {
        private string CreateTestData()
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var genre = new Genre { Name = "Genre 1" };

            var movies = new List<Movie>
            {
                new Movie {Title = "Movie 1", ReleaseDate = new DateTime(2010, 1, 1), InTheaters = false},
                new Movie {Title = "Movie not released", ReleaseDate = DateTime.Today.AddDays(1), InTheaters = false},
                new Movie {Title = "Movie in theaters", ReleaseDate = DateTime.Today.AddDays(-1), InTheaters = true}
            };

            var movieWithGenre = new Movie
            {
                Title = "Movie with Genre",
                ReleaseDate = new DateTime(2010, 1, 1),
                InTheaters = false
            };

            movies.Add(movieWithGenre);

            context.Add(genre);
            context.AddRange(movies);
            context.SaveChanges();

            var movieGenre = new MovieGenre
            {
                GenreId = genre.Id,
                MovieId = movieWithGenre.Id
            };

            context.Add(movieGenre);
            context.SaveChanges();

            return dbName;
        }

        [TestMethod]
        public async Task FilterByTitle()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            //Act
            var controller = new MoviesController(context, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var movieTitle = "Movie 1";

            var movieFilterDTO = new MovieFilterDTO
            {
                Title = movieTitle,
                PageSize = 10
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;
            Assert.AreEqual(1, movies.Count);
            Assert.AreEqual(movieTitle, movies[0].Title);
        }

        [TestMethod]
        public async Task FilterByInTheaters()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            //Act
            var controller = new MoviesController(context, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var movieFilterDTO = new MovieFilterDTO
            {
                InTheaters = true
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;
            Assert.AreEqual(1, movies.Count);
            Assert.AreEqual("Movie in theaters", movies[0].Title);
        }

        [TestMethod]
        public async Task FilterByNextReleases()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            //Act
            var controller = new MoviesController(context, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var movieFilterDTO = new MovieFilterDTO
            {
                NextReleases = true
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;
            Assert.AreEqual(1, movies.Count);
            Assert.AreEqual("Movie not released", movies[0].Title);
        }

        public async Task FilterByGenre()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            //Act
            var controller = new MoviesController(context, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var genreId = context.Genres.Select(x => x.Id).FirstOrDefault();

            var movieFilterDTO = new MovieFilterDTO
            {
                GenreId = genreId
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;
            Assert.AreEqual(1, movies.Count);
            Assert.AreEqual("Movie with Genre", movies[0].Title);
        }

        [TestMethod]
        public async Task FilterByTitleAscending()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            //Act
            var controller = new MoviesController(context, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var movieFilterDTO = new MovieFilterDTO
            {
                OrderBy = "Title",
                Ascending = true
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;

            var newContext = BuildContext(dbName);
            var moviesFromDB = newContext.Movies.OrderBy(x => x.Title).ToList();

            Assert.AreEqual(moviesFromDB.Count, movies.Count);

            for (int i = 0; i < moviesFromDB.Count; i++)
            {
                var movieFromController = movies[i];
                var movieFromDB = moviesFromDB[i];

                Assert.AreEqual(movieFromDB.Id, movieFromController.Id);
            }
        }

        [TestMethod]
        public async Task FilterByTitleDescending()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            //Act
            var controller = new MoviesController(context, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var movieFilterDTO = new MovieFilterDTO
            {
                OrderBy = "Title",
                Ascending = false
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;

            var newContext = BuildContext(dbName);
            var moviesFromDB = newContext.Movies.OrderByDescending(x => x.Title).ToList();

            Assert.AreEqual(moviesFromDB.Count, movies.Count);

            for (int i = 0; i < moviesFromDB.Count; i++)
            {
                var movieFromController = movies[i];
                var movieFromDB = moviesFromDB[i];

                Assert.AreEqual(movieFromDB.Id, movieFromController.Id);
            }
        }

        [TestMethod]
        public async Task FilterByWrongAttributeReturnsMovies()
        {
            //Arrange
            var dbName = CreateTestData();
            var mapper = BuildMapper();
            var context = BuildContext(dbName);

            var mock = new Mock<ILogger<MoviesController>>();

            //Act
            var controller = new MoviesController(context, mapper, null, mock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var movieFilterDTO = new MovieFilterDTO
            {
                OrderBy = "abcde",
                Ascending = true
            };

            var response = await controller.GetFilteredMovie(movieFilterDTO);

            //Assert
            var result = response as OkObjectResult;
            var movies = result.Value as List<MovieDTO>;

            var newContext = BuildContext(dbName);
            var moviesFromDB = newContext.Movies.ToList();

            Assert.AreEqual(moviesFromDB.Count, movies.Count);
            Assert.AreEqual(1, mock.Invocations.Count);
        }
    }
}