using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Movies.Api.Controllers;
using Movies.Api.DTO;

namespace Movies.UnitTests
{
    [TestClass]
    public class UsersControllerTests : BaseTests
    {
        [TestMethod]
        public async Task CreateUser()
        {
            var dbName = Guid.NewGuid().ToString();
            await CreateUserHelper(dbName);

            var newContext = BuildContext(dbName);
            var usersCount = await newContext.Users.CountAsync();

            Assert.AreEqual(1, usersCount);
        }

        [TestMethod]
        public async Task LoginWithWrongPassword()
        {
            var dbName = Guid.NewGuid().ToString();
            await CreateUserHelper(dbName);

            var controller = BuildUsersController(dbName);
            var userInfo = new UserInfo { Email = "example@gmail.com", Password = "WrongPassword" };

            var response = await controller.Login(userInfo);
            var result = response as BadRequestObjectResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task LoginWithCorrectPassword()
        {
            var dbName = Guid.NewGuid().ToString();
            await CreateUserHelper(dbName);

            var controller = BuildUsersController(dbName);
            var userInfo = new UserInfo { Email = "alfredo.2993@gmail.com", Password = "D4fault$Passw0rd!" };

            var response = await controller.Login(userInfo);
            var result = response as OkObjectResult;
            var userToken = result.Value as UserToken;

            Assert.IsNotNull(result.Value);
            Assert.IsNotNull(userToken);
        }

        private async Task CreateUserHelper(string dbName)
        {
            var controller = BuildUsersController(dbName);
            var userInfo = new UserInfo
            {
                Email = "alfredo.2993@gmail.com",
                Password = "D4fault$Passw0rd!"
            };

            await controller.Post(userInfo);
        }

        private UsersController BuildUsersController(string dbName)
        {
            var context = BuildContext(dbName);
            var userStore = new UserStore<IdentityUser>(context);
            var userManager = BuildUserManager(userStore);
            var mapper = BuildMapper();

            var httpContext = new DefaultHttpContext();
            MockAuthentication(httpContext);

            var signInManager = SetupSignInManager(userManager, httpContext);

            var myConfigurationSettings = new Dictionary<string, string>
            {
                {"Jwt:Key", "JwtSecurityTokenSecretKey"}
            };

            var configuration = new ConfigurationBuilder()
                                        .AddInMemoryCollection(myConfigurationSettings).Build();

            return new UsersController(userManager, signInManager, configuration, context, mapper);
        }

        private Mock<IAuthenticationService> MockAuthentication(HttpContext context)
        {
            var auth = new Mock<IAuthenticationService>();
            context.RequestServices = new ServiceCollection().AddSingleton(auth.Object).BuildServiceProvider();
            return auth;
        }

        // Source: https://github.com/dotnet/aspnetcore/blob/master/src/Identity/test/Shared/MockHelpers.cs
        // Source: https://github.com/dotnet/aspnetcore/blob/master/src/Identity/test/Identity.Test/SignInManagerTest.cs
        // Some code was modified to be adapted to our project.
        private UserManager<TUser> BuildUserManager<TUser>(IUserStore<TUser> store = null) where TUser : class
        {
            store = store ?? new Mock<IUserStore<TUser>>().Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions();
            idOptions.Lockout.AllowedForNewUsers = false;

            options.Setup(o => o.Value).Returns(idOptions);

            var userValidators = new List<IUserValidator<TUser>>();

            var validator = new Mock<IUserValidator<TUser>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<TUser>>();
            pwdValidators.Add(new PasswordValidator<TUser>());

            var userManager = new UserManager<TUser>(store, options.Object, new PasswordHasher<TUser>(),
                userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null,
                new Mock<ILogger<UserManager<TUser>>>().Object);

            validator.Setup(v => v.ValidateAsync(userManager, It.IsAny<TUser>()))
                .Returns(Task.FromResult(IdentityResult.Success)).Verifiable();

            return userManager;
        }

        private static SignInManager<TUser> SetupSignInManager<TUser>(UserManager<TUser> manager,
            HttpContext context, ILogger logger = null, IdentityOptions identityOptions = null,
            IAuthenticationSchemeProvider schemeProvider = null) where TUser : class
        {
            var contextAccessor = new Mock<IHttpContextAccessor>();
            contextAccessor.Setup(a => a.HttpContext).Returns(context);
            identityOptions = identityOptions ?? new IdentityOptions();
            var options = new Mock<IOptions<IdentityOptions>>();
            options.Setup(a => a.Value).Returns(identityOptions);
            var claimsFactory = new UserClaimsPrincipalFactory<TUser>(manager, options.Object);
            schemeProvider = schemeProvider ?? new Mock<IAuthenticationSchemeProvider>().Object;
            var sm = new SignInManager<TUser>(manager, contextAccessor.Object, claimsFactory, options.Object, null, schemeProvider, new DefaultUserConfirmation<TUser>());
            sm.Logger = logger ?? (new Mock<ILogger<SignInManager<TUser>>>()).Object;
            return sm;
        }
    }
}