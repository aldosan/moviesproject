using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Movies.Api.Controllers;
using Movies.Api.DTO;
using Movies.Api.Entities;
using NetTopologySuite;
using NetTopologySuite.Geometries;

namespace Movies.UnitTests
{
    [TestClass]
    public class CinemaRoomsControllerTests : BaseTests
    {
        [TestMethod]
        public async Task GetCinemaRoomsUntil5KM()
        {
            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);
            var filterDTO = new CinemaRoomDistanceFilterDTO
            {
                DistanceKms = 5,
                Latitude = 18.481139,
                Longitude = -69.938950
            };

            using (var context = LocalDbInitializer.GetDbContextLocalDb(false))
            {
                var mapper = BuildMapper();
                var controller = new CinemaRoomsController(context, mapper, geometryFactory);
                var response = await controller.GetByDistance(filterDTO);

                var result = response as OkObjectResult;
                var dtos = result.Value as List<CinemaRoomDistanceDTO>;
                Assert.AreEqual(2, dtos.Count);
            }
        }
    }
}