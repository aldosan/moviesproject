using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Newtonsoft.Json;

namespace Movies.IntegrationTests
{
    [TestClass]
    public class GenresControllerTests : BaseTests
    {
        private static readonly string _url = "/api/v1/genres";

        [TestMethod]
        public async Task GetAllGenresFromEmptyList()
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);
            var client = factory.CreateClient();

            var response = await client.GetAsync(_url);
            response.EnsureSuccessStatusCode();

            var genres = JsonConvert.DeserializeObject<List<GenreDTO>>(await response.Content.ReadAsStringAsync());
            Assert.AreEqual(0, genres.Count);
        }

        [TestMethod]
        public async Task GetAllGenres()
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);

            var context = BuildContext(dbName);
            context.Genres.Add(new Genre { Name = "Genre 1" });
            context.Genres.Add(new Genre { Name = "Genre 2" });
            await context.SaveChangesAsync();

            var client = factory.CreateClient();

            var response = await client.GetAsync(_url);
            response.EnsureSuccessStatusCode();

            var genres = JsonConvert.DeserializeObject<List<GenreDTO>>(await response.Content.ReadAsStringAsync());
            Assert.AreEqual(2, genres.Count);
        }

        [TestMethod]
        public async Task DeleteGenre()
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);

            var context = BuildContext(dbName);
            context.Genres.Add(new Genre { Name = "Genre 1" });
            await context.SaveChangesAsync();

            var client = factory.CreateClient();
            var response = await client.DeleteAsync($"{_url}/1");
            response.EnsureSuccessStatusCode();

            var newContext = BuildContext(dbName);
            var exists = await newContext.Genres.AnyAsync();

            Assert.IsFalse(exists);
        }

        [TestMethod]
        public async Task DeleteGenreReturns401()
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName, ignoreSecurity: false);

            var client = factory.CreateClient();
            var response = await client.DeleteAsync($"{_url}/1");
            Assert.AreEqual("Unauthorized", response.ReasonPhrase);
        }
    }
}