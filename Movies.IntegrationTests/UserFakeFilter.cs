using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Movies.IntegrationTests
{
    public class UserFakeFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            context.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.Email, "example@gmail.com"),
                new Claim(ClaimTypes.Name, "example@gmail.com"),
                new Claim(ClaimTypes.NameIdentifier, "26d664f4-17b7-4151-a3ec-22ee33ed4971"),
            }, "test"));

            await next();
        }
    }
}