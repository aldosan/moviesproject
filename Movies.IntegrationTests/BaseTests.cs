using Microsoft.EntityFrameworkCore;
using Movies.Api.Context;
using AutoMapper;
using NetTopologySuite;
using Movies.Api.Helpers;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Movies.Api;
using Microsoft.AspNetCore.TestHost;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;

namespace Movies.IntegrationTests
{
    public class BaseTests
    {
        protected string _defaultUserId = "26d664f4-17b7-4151-a3ec-22ee33ed4971";
        protected string _defaultUserEmail = "example@gmail.com";

        protected ApplicationDbContext BuildContext(string dbName)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                                .UseInMemoryDatabase(dbName).Options;

            var context = new ApplicationDbContext(options);
            return context;
        }

        protected IMapper BuildMapper()
        {
            var config = new MapperConfiguration(options =>
            {
                var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);
                options.AddProfile(new AutoMapperProfiles(geometryFactory));
            });

            return config.CreateMapper();
        }

        protected ControllerContext BuildControllerContext()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, _defaultUserEmail),
                new Claim(ClaimTypes.Email, _defaultUserEmail),
                new Claim(ClaimTypes.NameIdentifier, _defaultUserId)
            }));

            return new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = user }
            };
        }

        protected WebApplicationFactory<Startup> BuildWebApplicationFactory(string databaseName, bool ignoreSecurity = true)
        {
            var factory = new WebApplicationFactory<Startup>();
            factory = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var descriptorDBContext = services.SingleOrDefault(d =>
                                                        d.ServiceType == typeof(DbContextOptions<ApplicationDbContext>));

                    if (descriptorDBContext != null)
                    {
                        services.Remove(descriptorDBContext);
                    }

                    services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase(databaseName));

                    if (ignoreSecurity)
                    {
                        services.AddSingleton<IAuthorizationHandler, AllowAnonymousHandler>();

                        services.AddControllers(options =>
                        {
                            options.Filters.Add(new UserFakeFilter());
                        });
                    }
                });
            });
            return factory;
        }
    }
}