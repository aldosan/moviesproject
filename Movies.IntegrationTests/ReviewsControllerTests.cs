using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Movies.Api.DTO;
using Movies.Api.Entities;
using Newtonsoft.Json;

namespace Movies.IntegrationTests
{
    [TestClass]
    public class ReviewsControllerTests : BaseTests
    {
        private static readonly string _url = "/api/v1/movies/1/reviews";

        [TestMethod]
        public async Task GetReviewsWithNonExistentMovieReturns404()
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);

            var client = factory.CreateClient();
            var response = await client.GetAsync(_url);

            Assert.AreEqual(404, (int)response.StatusCode);
        }

        [TestMethod]
        public async Task GetReviewsFromEmptyList()
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);
            var context = BuildContext(dbName);

            context.Movies.Add(new Movie { Title = "Movie 1" });
            await context.SaveChangesAsync();

            var client = factory.CreateClient();
            var response = await client.GetAsync(_url);
            response.EnsureSuccessStatusCode();

            var reviews = JsonConvert.DeserializeObject<List<ReviewDTO>>(await response.Content.ReadAsStringAsync());
            Assert.AreEqual(0, reviews.Count);
        }
    }
}